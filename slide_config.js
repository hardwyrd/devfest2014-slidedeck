var SLIDE_CONFIG = {
  // Slide settings
  settings: {
    title: 'Google Drive API Basics<br>with Python and Flask',
    subtitle: 'A quick introduction to Google Drive API, Python, and Flask.',
    //eventInfo: {
    //  title: 'Google I/O',
    //  date: '6/x/2013'
    //},
    useBuilds: true, // Default: true. False will turn off slide animation builds.
    usePrettify: true, // Default: true
    enableSlideAreas: true, // Default: true. False turns off the click areas on either slide of the slides.
    enableTouch: true, // Default: true. If touch support should enabled. Note: the device must support touch.
    //analytics: 'UA-XXXXXXXX-1', // TODO: Using this breaks GA for some reason (probably requirejs). Update your tracking code in template.html instead.
    favIcon: 'images/google_developers_logo_tiny.png',
    fonts: [
      'Open Sans:regular,semibold,italic,italicsemibold',
      'Source Code Pro'
    ],
    //theme: ['mytheme'], // Add your own custom themes or styles in /theme/css. Leave off the .css extension.
  },

  // Author information
  presenters: [{
    name: 'Romar Mayer Micabalo',
    company: 'DevOps Sys Admin<br>Innovuze / AspenLabs',
    gplus: 'http://plus.google.com/+RomarMayerMicabalo',
    twitter: '@hardwyrd',
    www: 'http://about.me/rmr.micabalo',
    github: 'http://github.com/hardwyrd'
  }/*, {
    name: 'Second Name',
    company: 'Job Title, Google',
    gplus: 'http://plus.google.com/1234567890',
    twitter: '@yourhandle',
    www: 'http://www.you.com',
    github: 'http://github.com/you'
  }*/]
};

