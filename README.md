devfest2014-slidedeck
=====================

This is the slide deck for the Google Drive API + Python codelab for DevFest CDO 2014.

To run the presentation, just open index.html in any browser.
